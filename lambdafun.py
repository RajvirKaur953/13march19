def add(a,b):
    return a+b

o=add(5,10)
print(o)

s=lambda a:a
print(s('Hello There'))

# Anonymous Function
ad=lambda x,y,z:x+y+z
sum1=ad(100,20,349)
print('Sum by Lambda= ',sum1)
sum2=ad(1,2,3)
print('Sum by Lambda= ',sum2)