from functools import reduce
no=[10,2394,34,45,4567,2,12,887,3,1,56,7]

def evns(n):
    # if n%2==0:
    # if n%2==1:
        return True

# check=list(filter(evns,no))
# print(check)

# using lambda
mp=list(filter(lambda x:x%2==0,no))
print(mp)
mp=list(filter(lambda x:x%2==1,no))
print(mp)

# reduce function
# rd=reduce(lambda a,b:a+b,[1,2,3])
rd=reduce(lambda a,b:a+b,no)
print(rd)
