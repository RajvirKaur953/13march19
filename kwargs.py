# def person(name,age):
# def person(**kwargs):                                  #used for passing multiple args
    # return 'name= :{},age= :{}'.format(name,age)
    # return kwargs
    # for a in kwargs:
    #     # print(kwargs.keys())
    #     return '{} = {}'.format(a,kwargs[a])
    #     # print('ABCD')                             #it will not print cz return is final statement

    
# a=person(name='ABCD',age=25)
# b=person(name='python',age=30,hobby='reading')
# c=person(name='mann',age=30,hobby='reading',roll_no=1,clas='python')
# print(a)
# print(b)
# print(c)


def person(**kwargs): 
    for x,y in kwargs.items():
        print('{}={}'.format(x,y))
    print('\n')

person(name='ABCD',age=25)
person(name='python',age=30,hobby='reading')
person(name='mann',age=30,hobby='reading',roll_no=1,clas='python')